<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

$posts = fetch_all_posts();
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
            
            <ul>
                <?php foreach ($posts as $post) { ?>
                    <li>
                        <a href="posts/<?php echo $post['post_url'] . '/'; ?>"><?php echo $post['post_title']; ?></a> - <small>

                            posted <?php echo parse_sql_timestamp($post['post_date'], $format = 'F d\, Y \a\t H:i'); ?>
                            by <?php $user_id = $post['post_author']; $userdata = fetch_userdata($user_id); echo $userdata['display_name']; ?>.
                        </small>
                    </li>
                <?php } ?>
            </ul>
            
            <br />
            
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>