<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

require($_SERVER['DOCUMENT_ROOT'] . '/includes/lib/password.php');

if (isset($_SESSION['activation_needed'])) {
    if (isset($_POST['activation_key'])) {

        $id = $_SESSION['activation_needed']['user_id'];
        $user_activation_key = $_POST['activation_key'];

        if (empty($user_activation_key)) {

            $error = 'All fields are required!';

        } else {

            $query = $pdo->prepare("SELECT * FROM users WHERE user_id = ?");
            
            $query->bindValue(1, $id);
                
            $query->execute();
                
            $data = $query->fetch();

            $activation_key = $data['user_activation_key'];

                
            if ($activation_key == $user_activation_key) {

                $query = $pdo->prepare("UPDATE users SET user_activation_key = ? WHERE user_id = ?");

                $query->bindValue(1, '');
                $query->bindValue(2, $id);
                        
                $query->execute();


                unset($_SESSION['activation_needed']);

                header('Location: index.php');
                exit();

            } else {
                    
                $error = 'Wrong activation key!';
            }
        }
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

    <?php if (isset($error)) { ?>
        <small style="color:#aa0000;"><?php echo $error; ?></small>
        <br /><br />
    <?php } ?>
                    
    <form action="admin/index.php" method="post" autocomplete="off">
        <input type="text" name="activation_key" placeholder="Activation Key" /><br />
        <input type="submit" value="Activate Account" class="button" /><a href="admin/logout.php" class= "button">Logout</a>
    </form>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>

<?php

} else if (isset($_SESSION['logged_in'])) {

?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                    
    <ul>
        <li><a href="admin/add.php">Add Post</a></li>
        <li><a href="admin/edit-delete.php">Edit or Delete Posts</a></li>
        <li><a href="admin/edit-navigation.php">Edit Navigation</a></li>
        <li><a href="admin/logout.php">Logout</a></li>
    </ul>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>

<?php
} else {
    if (isset($_POST['userlogin'], $_POST['pass'])) {
        $userlogin = $_POST['userlogin'];
        $pass = $_POST['pass'];
        
        if (empty($userlogin) or empty($pass)) {
            $error = 'All fields are required!';
        } else {
            
            $query = $pdo->prepare("SELECT * FROM users WHERE user_login = ?");
            $query->bindValue(1, $userlogin);
            
            $query->execute();
            
            $data = $query->fetch();
            $hash = $data['user_pass'];

            $user_id = $data['user_id'];
            $activation_key = $data['user_activation_key'];
            
            if (password_verify($pass, $hash)) {

                if (empty($activation_key)) {

                    $_SESSION['logged_in']['user_id'] = $data['user_id'];
                    
                    header('Location: index.php');
                    exit();

                } else {
                
                    $_SESSION['activation_needed']['user_id'] = $user_id;

                    header('Location: index.php');
                    exit();

                }

            } else {
                
                $error = 'Incorrect details!';
            }
        }
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                    
    <?php if (isset($error)) { ?>
        <small style="color:#aa0000;"><?php echo $error; ?></small>
        <br /><br />
    <?php } ?>
                    
    <form action="admin/index.php" method="post" autocomplete="off">
        <input type="text" name="userlogin" placeholder="Username" value="<?php if (isset($_POST['userlogin'])) { echo $_POST['userlogin']; } ?>" /><br />
        <input type="password" name="pass" placeholder="Password" /><br />
        <input type="submit" value="Login" class="button" />
    </form>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
    
<?php
}
?>