<?php session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

if (isset($_SESSION['logged_in'])) {

    if(isset($_SESSION['post']['post_page_succes'])) {
            
    $post_name = $_SESSION['post']['post_name'];
    $post_title = $_SESSION['post']['post_title'];
            
    $newpage = 'Page creation succes!<br />Now let\'s make shure people find it..<br />Paste \'' . $post_title . ':' . $post_name . '.php\'' . ' in the navigation menu below and put \'>\' in front of it to make it a sub item.';
            
    unset($_SESSION['post']['post_name']);
    unset($_SESSION['post']['post_title']);
    unset($_SESSION['post']['post_page_succes']);
    }
    
    $filename = $_SERVER['DOCUMENT_ROOT'] . "/includes/navigation.txt";
    $fileHandle = fopen($filename, "r");
    $contents = fread($fileHandle, filesize($filename));
    fclose($fileHandle);
    
    
    if (isset($_POST['nav'])) {
    
        $nav = preg_replace("/[\r\n]+/", "\n", trim($_POST['nav']));
            
        if($nav == '') { 
            $error = 'Navigation can\'t be blanc!';
        } else {
    
            if($nav == $contents) { 
                $error = 'No changes were made!';
            } else {
        
                $fileHandle = fopen($_SERVER['DOCUMENT_ROOT'] . '/includes/navigation.txt', 'w') or die('can\'t open file');
                fwrite($fileHandle,$nav);
                fclose($fileHandle);
                            
                header('Location: ../admin/');
            
            }
        }
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
    
    <?php 
        if (isset($newpage)) {
            echo $newpage;
        }
    ?>

    <form action="admin/edit-navigation.php" method="post">
        
        navigation:<br /><br />
        
        <?php if (isset($error)) { ?>
            <small style="color:#aa0000;"><?php echo $error; ?></small>
            <br /><br />
        <?php } ?>

        <textarea name="nav" rows="15" cols="60" placeholder="Navigation" ><?php echo $contents; ?></textarea><br />
        
        <input type="submit" value="submit" class="button">
        
    </form>    
    
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>

<?php
} else {
    header('Location: index.php');
    exit();
}
?>