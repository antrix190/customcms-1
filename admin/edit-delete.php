<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

if (isset($_SESSION['logged_in'])) {

$posts = fetch_all_posts();

?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                
                <ul>
                    <?php foreach ($posts as $post) { ?>
                        <li>
                            <?php echo $post['post_title']; ?> 
                            <small>
                                <a href="admin/edit/<?php echo $post['post_url'] . '/'; ?>">edit</a> - 
                                <a href="admin/delete/<?php echo $post['post_url'] . '/'; ?>">delete</a>
                            </small>
                            
                            <?php /*- <small>
                               posted <?php echo date('F d\, Y \a\t H:i', $post['post_date']); ?> 
                            </small>*/ ?>
                        </li>
                    <?php } ?>
                </ul>
                
                <br />
                
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
    
<?php
    
} else {
    header('Location: index.php');
    exit();
}

?>