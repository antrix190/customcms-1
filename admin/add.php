<?php

session_start();


include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/Markdown.php');
    use \Michelf\Markdown;
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/HTMLPurifier.standalone.php');

if (isset($_SESSION['logged_in'])) {
    
    if(isset($_POST['title'], $_POST['content'])) {
        
        $title = htmlentities(preg_replace('!\s+!', ' ', trim($_POST['title'])), ENT_QUOTES, "UTF-8");
        $url = Slug($_POST['title']);
        
        $editable_content = $_POST['content'];
        $content = Markdown::defaultTransform($_POST['content']);
        
        $emptytesttitle = preg_replace('/[^A-Za-z0-9]/', '', trim($_POST['title']));
        $emptytestcontent = preg_replace('/[^A-Za-z0-9]/', '', trim($_POST['content']));
        
        
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
        $config->set('Cache.DefinitionImpl', null);
        
        $config->set('HTML.Allowed', 'a[href|title],blockquote[cite],p,ul,li,strong,em,pre,code,img[src]');
        
        $purifier = new HTMLPurifier($config);
        
        //$title = $purifier->purify($title);
        $content = $purifier->purify($content);
        //$editable_content = $purifier->purify($editable_content);
        

        if (empty($title) or empty($content) or empty($emptytesttitle) or empty($emptytestcontent)) {
            
            $error = 'All fields must be filled in!';

        } else {
            
            if (empty($url)) {
            
                $error = 'Title error!';

            } else {
                
                $query = $pdo->prepare('INSERT INTO posts (post_author, post_url, post_title, post_content, post_editable_content, post_date) VALUES (?, ?, ?, ?, ?, ?)');
                            
                $query->bindValue(1, $_SESSION['logged_in']['user_id']);
                $query->bindValue(2, $url);
                $query->bindValue(3, $title);
                $query->bindValue(4, $content);
                $query->bindValue(5, $editable_content);
                $query->bindValue(6, date("Y-m-d H:i:s"));
                            
                $query->execute();
                
                $count = $query->rowCount();
            
                if ($count == 0) {
                        
                    $err = $query->errorInfo();
                        
                    if (isset($err[1])) {
                        // 1062 - Duplicate entry
                        while ($err[1] == 1062) {
                            
                            $url = increment_string($url, '-');
                
                            $query = $pdo->prepare('INSERT INTO posts (post_author, post_url, post_title, post_content, post_editable_content, post_date) VALUES (?, ?, ?, ?, ?, ?)');
                            
                            $query->bindValue(1, $_SESSION['logged_in']['user_id']);
                            $query->bindValue(2, $url);
                            $query->bindValue(3, $title);
                            $query->bindValue(4, $content);
                            $query->bindValue(5, $editable_content);
                            $query->bindValue(6, date("Y-m-d H:i:s"));
                            
                            $query->execute();
                            
                            $count = $query->rowCount();

                            if ($count == 0) {

                                $err = $query->errorInfo();

                            } else {
                                
                                $err[1] = 'not1062';
                                header('Location: edit-delete.php');
                                exit();
                            }
                        }
                    }

                } else {
                    
                    header('Location: edit-delete.php');
                    exit();
                }
            }
        }
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                
                <h4>Add Post</h4>
                
                <?php if (isset($error)) { ?>
                    <small style="color: #aa0000;"><?php echo $error; ?></small>
                    <br /><br />
                <?php } ?>
                    
                <form action="admin/add.php" method="post" autocomplete="off">

                    <input type="text" name="title" size="40" maxlength="80" placeholder="Title" value="<?php if (isset($title)) { echo $title; } ?>" /><br />
                    <textarea name="content" rows="15" cols="60" id="markItUp" placeholder="Post content" ><?php if (isset($editable_content)) { echo $editable_content; } ?></textarea><br />
                    
                    <input type="submit" value="Add Post" class="button" />
                    
                </form>

                    <script type="text/javascript" >
                       $(document).ready(function() {
                          $("#markItUp").markItUp(mySettings);
                       });
                    </script>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>

<?php
} else {
    header('Location: index.php');
    exit();
}

?>