<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

require($_SERVER['DOCUMENT_ROOT'] . '/includes/lib/password.php');

if (isset($_SESSION['logged_in']) or isset($_SESSION['activation_needed'])) {

    header('Location: http://' . $_SERVER["SERVER_NAME"]);

} else {
    if (isset($_POST['userlogin'], $_POST['pass'])) {

        $userlogin = $_POST['userlogin'];
        $email = $_POST['email'];
        $pass = $_POST['pass'];
        $confirmpass = $_POST['confirmpass'];
        
        if (empty($userlogin) or empty($email) or empty($pass) or empty($confirmpass)) {

            $error = 'All fields are required!';

        } else {

            $error = array();

            if (strlen($userlogin) < 3) {

                $error[] = 'Login must be at least 3 characters long!';
            }

            if (!preg_match("/^[a-zA-Z0-9 -_]+$/", $userlogin)) {

                $error[] = 'Login contains invalid characters!';
            }

            if (strlen($pass) < 8) {

                $error[] = 'Password must be at least 8 characters long!';
            }

            if ($pass !== $confirmpass) {

                $error[] = 'Passwords don\'t match!';
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                
                $error[] = 'Email address is invalid!';
            }

            if($error) {

                $error = implode('<br />', $error);

            } else {

                $hash = password_hash($pass, PASSWORD_BCRYPT, ['cost' => 12]);
                $activation_key = generateRandomString(8);
                
                $query = $pdo->prepare('INSERT INTO users (user_login, user_pass, user_email, user_registered, user_activation_key) VALUES (?, ?, ?, ?, ?)');
                
                $query->bindValue(1, $userlogin);
                $query->bindValue(2, $hash);
                $query->bindValue(3, $email);
                $query->bindValue(4, date("Y-m-d H:i:s"));
                $query->bindValue(5, $activation_key);
                
                $query->execute();

                $count = $query->rowCount();
                
                if ($count == 0) {
                            
                    $err = $query->errorInfo();
                            
                    if (isset($err[1])) {
                        // 1062 - Duplicate entry
                        if ($err[1] == 1062) {
                                    
                            $error = 'Login or email already exists.';

                        } else {
                            
                            $error = 'Unknown error.';
                        }
                    }

                } else {
                        
                    header('Location: index.php');

                    $to  = $email;
                    $subject = 'CMS activation key';
                    $message = '<html><body>Log in and enter your activation key: <strong>' . $activation_key . '</strong></body></html>';
                    
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: CMS <cms@example.com>' . "\r\n";

                    mail($to, $subject, $message, $headers);

                    exit();
                }
            }
        }
    }

?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                    
                    <?php if (isset($error)) { ?>
                        <small style="color:#aa0000;"><?php echo $error; ?></small>
                        <br /><br />
                    <?php } ?>
                    
                    <form action="admin/register.php" method="post" autocomplete="off">

                        <input type="text" name="userlogin" placeholder="Username" maxlength="60" value="<?php if (isset($_POST['userlogin'])) { echo $_POST['userlogin']; } ?>" /><br />

                        <input type="email" name="email" placeholder="Email" maxlength="100" value="<?php if (isset($_POST['email'])) { echo $_POST['email']; unset($_POST['email']); } ?>" /><br />
                        
                        <input type="password" name="pass" id="pass" placeholder="Password" maxlength="60" /><br />

                        <input type="password" name="confirmpass" id="confirmpass" placeholder="Confirm Password" /><br />
                        
                        <div id="passstrength"></div><br />
                        <input type="submit" value="Register" class="button" />
                    </form>

                    <script type="text/javascript">
                    $(function() {

                        $('#passstrength').html('Strength indicator');
                        $('#passstrength').css('display','block');

                        $('#pass, #confirmpass').keyup(function(e) {

                            if($('#pass').val() != '') {

                                if($('#pass').val() != '' && $('#confirmpass').val() != '' && $('#pass').val() != $('#confirmpass').val()) {

                                    $('#passstrength').removeClass().addClass('pass-error');
                                    $('#passstrength').html('Mismatch!');
                                    return false;
                                }

                                var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
                                var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
                                var enoughRegex = new RegExp("(?=.{8,}).*", "g");
                                
                                if (false == enoughRegex.test($('#pass').val())) {
                                    $('#passstrength').removeClass()/*.addClass('pass-error')*/;
                                    $('#passstrength').html('Too short!');
                                } else if (strongRegex.test($('#pass').val())) {
                                    $('#passstrength').removeClass().addClass('pass-ok');
                                    $('#passstrength').html('Strong');
                                } else if (mediumRegex.test($('#pass').val())) {
                                    $('#passstrength').removeClass().addClass('pass-alert');
                                    $('#passstrength').html('Medium');
                                } else {
                                    $('#passstrength').removeClass().addClass('pass-error');
                                    $('#passstrength').html('Weak!');
                                }
                            } else {
                                $('#passstrength').removeClass();
                                $('#passstrength').html('Strength indicator');
                            }
                            return true;
                        });
                    });
                    </script>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
    
<?php
}
?>