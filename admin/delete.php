<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

if (isset($_SESSION['logged_in'])) {

    if (isset($_GET['url'])) {
        
        $url = $_GET['url'];
        $data = fetch_postdata($url);
        
        $user_id = $data['post_author'];
        $userdata = fetch_userdata($user_id);    
        
        if (isset($_POST['delete'])) { 
        
            $query = $pdo->prepare("DELETE FROM posts WHERE post_url = ?");

            $query->bindValue(1, $url);
                    
            $query->execute();
                    
            header('Location: ../../edit-delete.php');
            exit();
        
        }
        
        ?>
    
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                        
                        <form action="admin/delete/<?php echo $url . "/"; ?>" method="post" autocomplete="off">
                            
                            Are you shure you want to delete this article?&nbsp;&nbsp;
                            <input type="submit" value="Yes" name="delete" class="button" />&nbsp;&nbsp;&nbsp;&nbsp;
                            <small><a href="admin/edit-delete.php">No</a></small>
                            
                        </form>
                        
                        <h4>
                            <?php echo $data['post_title']; ?> - 

                            <small>
                                posted <?php echo parse_sql_timestamp($data['post_date'], $format = 'F d\, Y \a\t H:i'); ?>
                                by <?php echo $userdata['display_name']; ?>
                            </small>
                            
                        </h4>
                            
                        <p><?php echo $data['post_content']; ?></p>
                        
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
    
        <?php
    } else {
        header('Location: edit-delete.php');
        exit();
    }
    
} else {
    header('Location: index.php');
    exit();
}

?>