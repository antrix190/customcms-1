<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/Markdown.php');
    use \Michelf\Markdown;
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/HTMLPurifier.standalone.php');

if (isset($_SESSION['logged_in'])) {
    if (isset($_GET['url'])) {
        
        $url = $_GET['url'];
        $data = fetch_postdata($url);
        
        $user_id = $data['post_author'];
        $userdata = fetch_userdata($user_id);
        
        if(isset($_POST['title'], $_POST['content'])) {
            
        $title = htmlentities(preg_replace('!\s+!', ' ', trim($_POST['title'])), ENT_QUOTES);
            
        $editable_content = $_POST['content'];
        $content = Markdown::defaultTransform($_POST['content']);
            
        $emptytesttitle = preg_replace('/[^A-Za-z0-9]/', '', trim($_POST['title']));
        $emptytestcontent = preg_replace('/[^A-Za-z0-9]/', '', trim($_POST['content']));
            
        
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
        $config->set('Cache.DefinitionImpl', null);
        
        $config->set('HTML.Allowed', 'a[href|title],blockquote[cite],p,ul,li,strong,em,pre,code,img[src]');
        
        $purifier = new HTMLPurifier($config);
        
        //$title = $purifier->purify($title);
        $content = $purifier->purify($content);
        $editable_content = $purifier->purify($editable_content);
            
            if (empty($title) or empty($content) or empty($emptytesttitle) or empty($emptytestcontent)) {
                
                $error = 'All fields must be filled in!';

            } else {
                
                if ($title == $data['post_title'] and $content == $data['post_content']) {
                    $error = 'No changes were made!';
                    
                } else {
                    
                    $query = $pdo->prepare("UPDATE posts SET post_title = ?, post_content = ?, post_editable_content = ?, post_modified = ? WHERE post_url = ?");
                    
                    $query->bindValue(1, $title);
                    $query->bindValue(2, $content);
                    $query->bindValue(3, $editable_content);
                    $query->bindValue(4, date("Y-m-d H:i:s"));
                    $query->bindValue(5, $url);
                    
                    $query->execute();
                    
                    $count = $query->rowCount();
            
                    if ($count == 0) {
                        
                        $err = $query->errorInfo();
                        
                        if (isset($err[1])) {
                            // 1062 - Duplicate entry
                            if ($err[1] == 1062) {
                                
                                $error = 'Post title already exists.';
                            } else {
                        
                                $error = 'Unknown error.';
                            }
                        }

                    } else {
                    
                        header('Location: ../../edit-delete.php');
                        exit();
                    }
                }
            }
        }
            
?>
        
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                            
                <h4>Edit Post</h4>
                            
                <?php if (isset($error)) { ?>
                    <small style="color:#aa0000;"><?php echo $error; ?></small>
                    <br /><br />
                <?php } ?>
                        
                <form action="admin/edit/<?php echo $url; ?>/" method="post" autocomplete="off">
                    <input type="text" name="title" size="40" maxlength="80" placeholder="Title" value="<?php if (isset($title)) { echo $title; } else { echo $data['post_title']; } ?>" /> - <small>posted <?php echo parse_sql_timestamp($data['post_date'], $format = 'F d\, Y \a\t H:i'); ?>
                    by <?php echo $userdata['display_name']; ?></small><br />
                    
                    <textarea name="content" rows="15" cols="60" id="markItUp" placeholder="Post content" ><?php if (isset($editable_content)) { echo $editable_content; } else { echo $data['post_editable_content']; } ?></textarea>

                    <script type="text/javascript" >
                       $(document).ready(function() {
                          $("#markItUp").markItUp(mySettings);
                       });
                    </script>
                    
                    <input type="submit" value="Edit Post" class="button" />
                </form>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
        
<?php
    
    } else {
        header('Location: edit-delete.php');
        exit();
    }
        
} else {
    header('Location: index.php');
    exit();
}
?>