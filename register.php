<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

if (isset($_SESSION['logged_in']) or isset($_SESSION['activation_needed'])) {

    header('Location: http://' . $_SERVER["SERVER_NAME"]);
    exit();
}

?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

<?php if (isset($error)) { ?>
    <small style="color:#aa0000;"><?php echo $error; ?></small>
    <br /><br />
<?php } ?>

<div class="done"><b>Thank you !</b> We have received your message.</div>

<form action="includes/process-register.php" method="post" autocomplete="off" class="form">
    
    <label for="login">Login</label>
    <input type="text" name="login" id="login" placeholder="Login" maxlength="60" value="<?php if (isset($_SESSION['login'])) { echo $_SESSION['login']; unset($_SESSION['login']); } ?>" />
    <div class="error"><label for="login" id="login_error"></label></div>

    <br />
    
    <label for="email">Email</label>
    <input type="text" name="email" id="email" placeholder="Email" maxlength="100" value="<?php if (isset($_SESSION['email'])) { echo $_SESSION['email']; unset($_SESSION['email']); } ?>" />
    <label class="error" for="email" id="email_error"></label>

    <br />
    
    <label for="pass">Password</label>
    <input type="password" name="pass" id="pass" placeholder="Password" maxlength="60" /><div class="error"><label class="error" for="pass" id="pass_error"></label></div>

    <br />
    
    <label for="confirmpass">Confirm Password</label>
    <input type="password" name="confirmpass" id="confirmpass" placeholder="Confirm Password" />
    <label class="error" for="confirmpass" id="confirmpass_error"></label>

    <br />
    
    <div id="passstrength"></div><br />
    <input type="submit" id="submit" value="Register" class="button" />
    <div class="loading">Loading</div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    validate_register();
});
</script>
  

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>