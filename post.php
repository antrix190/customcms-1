<?php

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

if (isset($_GET['url'])) {
    
    $url = $_GET['url'];
    $data = fetch_postdata($url);
    
    $user_id = $data['post_author'];
    $userdata = fetch_userdata($user_id);
    ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>
                    
    <h4>
        <?php echo $data['post_title']; ?> -
        <small>
            posted <?php echo parse_sql_timestamp($data['post_date'], $format = 'F d\, Y \a\t H:i'); ?>
            by <?php echo $userdata['display_name']; ?>
        </small>
    </h4>
                    
    <p><?php echo $data['post_content']; ?></p>
                    
    <a href="index.php">&larr; Back</a>
                    
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>

    <?php
} else {
    header('Location: index.php');
    exit();
}

?>