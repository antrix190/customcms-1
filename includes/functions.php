<?php
function  Navigation() {

    $rows = file($_SERVER['DOCUMENT_ROOT'] . '/includes/navigation.txt');
    echo '<ul id="page-navigation">';
    foreach($rows as $key => &$row) {
        
        $nav = explode(":", $row);
        
        if(isset($rows[$key-1])) {
            if(strpos($rows[$key-1],'>') !== false) { // > is found
                if(strpos($rows[$key-1],'>>') !== false) { // >> is found
                    $prevRowIsSubOfSub = 'true';
                } else {
                    $prevRowIsSub = 'true';
                }
            }
        }
        
        if(isset($rows[$key+1])) {
            if(strpos($rows[$key+1],'>') !== false) { // > is found
                if(strpos($rows[$key+1],'>>') !== false) { // >> is found
                    $nextRowIsSubOfSub = 'true';
                } else {
                    $nextRowIsSub = 'true';
                }
            }
        }
        
        if(strpos($row,'>') !== false) {

            if(strpos($row,'>>') !== false) { // >> is found

                if(!isset($prevRowIsSubOfSub)) { echo '<ul>'; }
                
                $page = str_replace('>>', '', trim($nav[0]));
                $link = trim($nav[1]);
                
                if($page === 'login' and $link === 'logout') {

                    if(isset($_SESSION['logged_in'])) {

                        echo '<li><a href="admin/logout.php">Logout</a></li>';

                    } else {

                        echo '<li><a href="admin/index.php">Login</a></li>';
                    }
                } else {
                    
                    echo '<li><a href="' . $link .  '">' . $page . '</a></li>';
                }
                
                if(!isset($nextRowIsSubOfSub)) { echo '</ul></li>'; }
                if(!isset($nextRowIsSub)) { echo '</ul></li>'; }

            } else { // > is found
                
                if(!isset($prevRowIsSub) and !isset($prevRowIsSubOfSub)) { echo '<ul>'; }
                
                $page = str_replace('>', '', trim($nav[0]));
                $link = trim($nav[1]);

                if($page === 'login' and $link === 'logout') {

                    if(isset($_SESSION['logged_in'])) {

                        echo '<li><a href="admin/logout.php">Logout</a></li>';

                    } else {

                        echo '<li><a href="admin/index.php">Login</a></li>';
                    }
                } else {
                    
                    if(isset($nextRowIsSubOfSub)) { echo '<li><a href="' . $link .  '">' . $page . '</a>'; }
                    if(!isset($nextRowIsSubOfSub)) { echo '<li><a href="' . $link .  '">' . $page . '</a></li>'; }
                }
                
                if(!isset($nextRowIsSub) and !isset($nextRowIsSubOfSub)) { echo '</ul></li>'; }
            }
            
        } else {
        
            $page = trim($nav[0]);
            $link = trim($nav[1]);

                if($page === 'login' and $link === 'logout') {

                    if(isset($_SESSION['logged_in'])) {

                        echo '<li><a href="admin/logout.php">Logout</a></li>';

                    } else {

                        echo '<li><a href="admin/index.php">Login</a></li>';
                    }
                } else {
                    
                    if(isset($nextRowIsSub)) { echo '<li><a href="' . $link .  '">' . $page . '</a>'; }
                    if(!isset($nextRowIsSub)) { echo '<li><a href="' . $link .  '">' . $page . '</a></li>'; }
                }
        }
        unset($page);
        unset($link);
        unset($nextRowIsSub);
        unset($prevRowIsSub);
        unset($nextRowIsSubOfSub);
        unset($prevRowIsSubOfSub);
    }
    echo '</ul>';
}

function Slug($string)
{
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}

function fetch_userdata($user_id) {
    global $pdo;
            
    $query = $pdo->prepare("SELECT * FROM users WHERE user_id = ?");
    $query->bindValue(1, $user_id);
    $query->execute();
            
    return $query->fetch();
}

function fetch_all_posts() {
    global $pdo;
        
    $query = $pdo->prepare("SELECT * FROM posts WHERE post_status = ?");
    $query->bindValue(1, 'publish');
    $query->execute();
        
    return $query->fetchAll();
}

function fetch_postdata($post_url) {
        global $pdo;
        
        $query = $pdo->prepare("SELECT * FROM posts WHERE post_url = ?");
        $query->bindValue(1, $post_url);
        $query->execute();
        
        return $query->fetch();
}

function increment_string($str, $separator = '_', $first = 2) {
    preg_match('/(.+)'.$separator.'([0-9]+)$/', $str, $match);

    return isset($match[2]) ? $match[1].$separator.($match[2] + 1) : $str.$separator.$first;
}

function parse_sql_timestamp($timestamp, $format = 'd-m-Y') {
    $date = new DateTime($timestamp);
    return $date->format($format);
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
?>