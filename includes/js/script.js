
/*------------------------------------//
//     Process The Register Page      //
//------------------------------------*/

function validate_register() {

    var login = $('input[name=login]');
    var email = $('input[name=email]');
    var pass = $('input[name=pass]');
    var confirmpass = $('input[name=confirmpass]');


    var user_lenght = new RegExp("(?=.{3,}).*");
    var user_characters = new RegExp(/[A-Za-z0-9_ -]/);
    var email_test = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    var pass_lenght = new RegExp("(?=.{8,}).*");

    
    var timer; // external so it's value is held over all instances of the timer function


    $("input[name='login']").on('input',function(){
        if (timer) {
            clearTimeout(timer);
        }
        login.removeClass('hightlight');
        $('#login_error').hide();

        timer = setTimeout(function() {
            if (login.val()=='') {
                login.addClass('hightlight');
                $('#login_error').show();
            } else {
                if (false == user_lenght.test($('#login').val())) {
                    login.addClass('hightlight');
                    $('#login_error').show();
                } else if (false == user_characters.test($('#login').val())) {
                    login.addClass('hightlight');
                    $('#login_error').show();
                } else {
                    login.removeClass('hightlight');
                    $('#login_error').hide();
                }
            }
        },1000);
    });

    $("input[name='email']").on('input',function(){
        if (timer) {
            clearTimeout(timer);
        }
        email.removeClass('hightlight');
        $('#email_error').hide();
        
        timer = setTimeout(function() {
            if (email.val()=='') {
                email.addClass('hightlight');
                $('#email_error').show();
            } else {
                if (false == email_test.test($('#email').val())) {
                    email.addClass('hightlight');
                    $('#email_error').show();
                } else {
                    email.removeClass('hightlight');
                    $('#email_error').hide();
                }
            }
        },1000);
    });

    $("input[name='pass']").on('input',function(){
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            if (pass.val()=='') {
                pass.addClass('hightlight');
                $('#pass_error').show();
            } else {
                pass.removeClass('hightlight');
                $('#pass_error').hide();
            }
        },1000);
    });

    $("input[name='confirmpass']").on('input',function(){
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            if (confirmpass.val()=='') {
                confirmpass.addClass('hightlight');
                $('#confirmpass_error').show();
            } else {
                confirmpass.removeClass('hightlight');
                $('#confirmpass_error').hide();
            }
        },1000);
    });


    $(function() {

        

        $('#pass, #confirmpass').keyup(function(e) {

            $('#passstrength').html('Strength indicator');
            $('#passstrength').fadeIn('slow');
            $('#passstrength').css('display','block');

            if($('#pass').val() != '') {

                if($('#pass').val() != '' && $('#confirmpass').val() != '' && $('#pass').val() != $('#confirmpass').val()) {

                    $('#passstrength').removeClass().addClass('pass-error');
                    $('#passstrength').html('Mismatch!');
                    return false;
                }

                var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
                var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
                var enoughRegex = new RegExp("(?=.{8,}).*", "g");
                                
                if (false == enoughRegex.test($('#pass').val())) {
                    $('#passstrength').removeClass()/*.addClass('pass-error')*/;
                    $('#passstrength').html('Too short!');
                } else if (strongRegex.test($('#pass').val())) {
                    $('#passstrength').removeClass().addClass('pass-ok');
                    $('#passstrength').html('Strong');
                } else if (mediumRegex.test($('#pass').val())) {
                    $('#passstrength').removeClass().addClass('pass-alert');
                    $('#passstrength').html('Medium');
                } else {
                    $('#passstrength').removeClass().addClass('pass-error');
                    $('#passstrength').html('Weak!');
                }
            } else {
                $('#passstrength').removeClass();
                $('#passstrength').fadeOut('slow');
            }
            return true;
        });
    });



    ////////////////////////////////////
    //   WHEN THE FORM IS SUBMITTED   //
    ////////////////////////////////////

    $('#submit').on('click', function () {   
        
        //Get the data from all the fields
        var login = $('input[name=login]');
        var email = $('input[name=email]');
        var pass = $('input[name=pass]');
        var confirmpass = $('input[name=confirmpass]');

        //Simple validation to make sure user entered something
        //If error found, add hightlight class to the text field
        if (login.val()=='') {
            login.addClass('hightlight');
            $("#login_error").show();
            $("#login_error").html("This field is required!");
            var error = true;
        } else {
            login.removeClass('hightlight');
            $("#login_error").hide();
        }
        
        if (email.val()=='') {
            email.addClass('hightlight');
            $("#email_error").show();
            $("#email_error").html("This field is required!");
            var error = true;
        } else {
            email.removeClass('hightlight');
            $("#email_error").hide();
        }

        if (pass.val()=='') {
            pass.addClass('hightlight');
            $("#pass_error").show();
            $("#pass_error").html("This field is required!");
            var error = true;
        } else {
            pass.removeClass('hightlight');
            $("#pass_error").hide();
        }

        if (confirmpass.val()=='') {
            confirmpass.addClass('hightlight');
            $("#confirmpass_error").show();
            $("#confirmpass_error").html("This field is required!");
            var error = true;
        } else {
            confirmpass.removeClass('hightlight');
            $("#confirmpass_error").hide();
        }

        if ($(error).length) {
            return false;
        }

        
        var data = $("form").serialize();

        //disabled all the text fields
        //Note: serialize() doesn't process disabled elements
        $('input').attr('disabled','true');
        
        //show the loading sign
        $('.loading').show();
        
        //start the ajax
        $.ajax({
            //this is the php file that processes the data and send mail
            url: "../process-register.php", 
            
            //GET method is used
            type: "GET",

            //pass the data
            data: data,

            //Do not cache the page
            cache: false,
            
            //success
            success: function (html) {
                //if process.php returned 1/true (send mail success)
                if (html == 1) {                  
                    //hide the form
                    $('.form').fadeOut('slow');                 
                    
                    //show the success message
                    $('.done').fadeIn('slow');
                    
                //if process.php returned 0/false (send mail failed)
                } else {
                    $('input').removeAttr('disabled');
                    $('.loading').hide();
                    alert('Sorry, unexpected error. Please try again later.');
                }
            }       
        });
        
        //cancel the submit button default behaviours
        return false;
    }); 
}





                    /*------------------------------------//
                    //        MarkItUp LiveReload         //
                    //------------------------------------*/

/*                    <script type="text/javascript" >
                       $(document).ready(function() {
                          $("#markItUp").markItUp(mySettings);
                       });
                    </script>

                    <script src="http://pagedown.googlecode.com/hg/Markdown.Converter.js"></script>
                    <script src="http://pagedown.googlecode.com/hg/Markdown.Sanitizer.js"></script>
                    <script>
                         (function() {
                          // When using more than one `textarea` on your page, change the following line to match the one you’re after
                          var textarea = document.getElementsByTagName('textarea')[0],
                              preview = document.createElement('div'),
                              convert = new Markdown.getSanitizingConverter().makeHtml;
                          function update() {
                           preview.innerHTML = convert(textarea.value);
                          }
                          // Continue only if the `textarea` is found
                          if (textarea) {
                           preview.id = 'preview-content';
                           // Insert the preview `div` after the `textarea`
                           textarea.parentNode.insertBefore(preview, textarea.nextSibling);
                           textarea.oninput = function() {
                            textarea.onkeyup = null;
                            update();
                           };
                           textarea.onkeyup = update;
                           // Trigger the `onkeyup` event
                           textarea.onkeyup.call(textarea);
                          };
                         }());
                    </script>*/