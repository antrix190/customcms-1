<?php

session_start();

include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/connection.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

require($_SERVER['DOCUMENT_ROOT'] . '/includes/lib/password.php');

//Retrieve form data. 
//GET - user submitted data using AJAX
//POST - in case user does not support javascript, we'll use POST instead

if ($_POST) {

    $login = $_POST['login']; $_SESSION['login'] = $login;
    $email = $_POST['email']; $_SESSION['email'] = $email;
    $pass = $_POST['pass'];
    $confirmpass = $_POST['confirmpass'];

} else {

    $login = ($_GET['login']);
    $email = ($_GET['email']);
    $pass = ($_GET['pass']);
    $confirmpass = ($_GET['confirmpass']);

}


//Simple server side validation for POST data, of course, 
//you should validate the email
$field_error = array();
$error = null;

if (empty($login)) $field_error[] = 'login';
if (empty($email)) $field_error[] = 'email'; 
if (empty($pass)) $field_error[] = 'password';
if (empty($confirmpass)) $field_error[] = 'password confirmation';

if (strlen($login) < 3) {

    $error .= 'Login must be at least 3 characters long!|';
}

if (!empty($login)) {
    if (!preg_match('/[A-Za-z0-9_ -]/', $login)) {

        $error .= 'Login contains invalid characters!|';
    }
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                
    $error .= 'Email address is invalid!|';
}

if (strlen($pass) < 8) {

    $error .= 'Password must be at least 8 characters long!|';
}

if ($pass !== $confirmpass) {

    $error .= 'Passwords don\'t match!|';
}


//if the errors array is empty, send the mail
if (empty($error) and empty($field_error)) {

                $hash = password_hash($pass, PASSWORD_BCRYPT, ['cost' => 12]);
                $activation_key = generateRandomString(8);
                
                $query = $pdo->prepare('INSERT INTO users (user_login, user_pass, user_email, user_registered, user_activation_key) VALUES (?, ?, ?, ?, ?)');
                
                $query->bindValue(1, $login);
                $query->bindValue(2, $hash);
                $query->bindValue(3, $email);
                $query->bindValue(4, date("Y-m-d H:i:s"));
                $query->bindValue(5, $activation_key);
                
                $query->execute();

                $count = $query->rowCount();
                
                if ($count == 0) {
                            
                    $err = $query->errorInfo();
                            
                    if (isset($err[1])) {
                        // 1062 - Duplicate entry
                        if ($err[1] == 1062) {
                                    
                            $error = 'Login or email already exists.';

                        } else {
                            
                            $error = 'Sorry, unknown error. Please try again later.';

                        }
                    }

                    $result = 0;

                } else {

                    $to  = $email;
                    $subject = 'CMS activation key';
                    $message = '<html><body>Log in and enter your activation key: <strong>' . $activation_key . '</strong></body></html>';
                    
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: CMS <cms@example.com>' . "\r\n";

                    if (mail($to, $subject, $message, $headers)) {
                        $result = 1;
                    } else {
                        $result = 0;
                        $error = 'Could not send the mail with your activation key. Please try again later.';
                    }
                    unset($_SESSION['login']);
                    unset($_SESSION['email']);
                }
    
    //if POST was used, display the message straight away
    if ($_POST) {
        if ($result == 1) {
            $success_message = 'Registration completed! Check your inbox for the mail with the activation key.';
        } else {
            $error_message = $error;
        }

        ob_start();
        include $_SERVER['DOCUMENT_ROOT'] . '/includes/message.php';
        $message_page = ob_get_contents();
        ob_end_clean();

        if (get_magic_quotes_gpc()) {
            $message_page = stripslashes($message_page);
        } else { 
            $message_page = $message_page;
        }

        echo $message_page;
        exit;
        
    //else if GET was used, return the boolean value so that 
    //ajax script can react accordingly
    //1 means success, 0 means failed
    } else {
        echo $result;
    }

//if the errors array has values
} else {

    //Find out the key of the last element in the array
    //Then set the pointer back to at the start of the array
    if (!empty($field_error)) {
        end($field_error);
        $end_id = key($field_error);
        reset($field_error);

        //display the errors message
        if ($end_id == 0) {
            $error_message = 'The following field is required: <em>';
        } else {
            $error_message = 'The following fields are required: <em>';
        }

        for ($i=0; $i<count($field_error); $i++) {
            if ($i == $end_id) {
                $error_message .= $field_error[$i];
            } else {
                $error_message .= $field_error[$i] . ', ';
            }
        }
        $error_message .= '</em>.';
    }

    if (!empty($error)) {
        if (!empty($field_error)) {
            $error_message .= '|' . $error;
        } else {
            $error_message = $error;
        }
    }

    ob_start();
    include $_SERVER['DOCUMENT_ROOT'] . '/includes/message.php';
    $error_page = ob_get_contents();
    ob_end_clean();

    if (get_magic_quotes_gpc()) {
        $error_page = stripslashes($error_page);
    } else { 
        $error_page = $error_page;
    }

    echo $error_page;
    exit;
}


//Simple mail function with HTML header
function sendmail($to, $subject, $message, $from) {
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
    $headers .= 'From: ' . $from . "\r\n";
    
    $result = mail($to,$subject,$message,$headers);
    
    if ($result) return 1;
    else return 0;
}
?>