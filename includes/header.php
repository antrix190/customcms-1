<!DOCTYPE html>
<html>
    <head>
        <base href="http://<?php echo $_SERVER["SERVER_NAME"]; ?>" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">

        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
        <title>Custom CMS</title>
        
        <link rel="stylesheet" type="text/css" href="includes/css/style.css" />
        <link rel="stylesheet" type="text/css" href="includes/css/normalize.css" />
        <link rel="stylesheet" href="includes/markitup/sets/markdown/style.css" />
        
        <script type="text/javascript" src="includes/js/script.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="includes/markitup/jquery.markitup.js"></script>
        <script type="text/javascript" src="includes/markitup/sets/markdown/set.js"></script>
        
        <link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
        
    </head>
    
    <body>

        <!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
            
        <div id="page-wrap">

            <nav>
                <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');
                Navigation();
                ?>
            </nav>

            <div id="container">